/*
 * I2C.c
 *
 *      Author: Embedownik
 */

#include <I2C.h>

#include <FreeRTOS.h>
#include <semphr.h>

#ifndef I2C_DEF_TIMEOUT_SINGLE_BYTE
#warning "no I2C_DEF_TIMEOUT_SINGLE_BYTE define from board.h - default value - (2U)"
#define I2C_DEF_TIMEOUT_SINGLE_BYTES (2U)
#endif

//! Mutex for protecting I2C periph access
static SemaphoreHandle_t xI2CMutex;

//! pointer for I2C instace from init
static I2C_HandleTypeDef *I2CHandler;

static void I2C_lock(void)
{
    xSemaphoreTake(xI2CMutex, portMAX_DELAY);
//	vTaskSuspendAll(); /* for tests in case problems with I2C */
}

static void I2C_unlock(void)
{
//	xTaskResumeAll(); /* for tests in case problems with I2C */
    xSemaphoreGive(xI2CMutex);
}

/**
 * Calculate "expected" response time for transmitting/receiving data.
 * TODO - implement calculations based on I2C speed.
 */
static uint16_t calculateTimeout(uint16_t dataSize)
{
    return (dataSize/10U) + 10U;
}

bool I2C_init(I2C_HandleTypeDef *handler)
{
    bool initSuccess = false;

    I2CHandler = handler;

    xI2CMutex = xSemaphoreCreateMutex();

    if(xI2CMutex != NULL)
    {
        initSuccess = true;
    }

    return initSuccess;
}

bool I2C_readByte(uint8_t devAddress, uint8_t regAddress, uint8_t *value)
{
    bool retVal = true;

    I2C_lock();

    HAL_StatusTypeDef comStatus = HAL_I2C_Mem_Read(I2CHandler, devAddress, regAddress, I2C_MEMADD_SIZE_8BIT, value, 1U, I2C_DEF_TIMEOUT_SINGLE_BYTES);

    if(comStatus != HAL_OK)
    {
        retVal = false;
    }

    I2C_unlock();

    return retVal;
}

bool I2C_readBytes(uint8_t devAddress, uint8_t regAddress, uint8_t *value, uint8_t bytes)
{
    bool retVal = true;

    I2C_lock();

    HAL_StatusTypeDef comStatus = HAL_I2C_Mem_Read(I2CHandler, devAddress, regAddress, I2C_MEMADD_SIZE_8BIT, value, bytes, calculateTimeout(bytes));

    if(comStatus != HAL_OK)
    {
        retVal = false;
    }

    I2C_unlock();

    return retVal;
}

bool I2C_writeByte(uint8_t devAddress, uint8_t regAddress, uint8_t value)
{
    bool retVal = true;

    I2C_lock();

    HAL_StatusTypeDef comStatus = HAL_I2C_Mem_Write(I2CHandler, devAddress, regAddress, I2C_MEMADD_SIZE_8BIT, &value, 1U, I2C_DEF_TIMEOUT_SINGLE_BYTES);

    if(comStatus != HAL_OK)
    {
        retVal = false;
    }

    I2C_unlock();

    return retVal;
}

bool I2C_writeBytes(uint8_t devAddress, uint8_t regAddress, const uint8_t *values, uint16_t size)
{
    bool retVal = true;

    I2C_lock();

    /* because of lack of consts ST HAL library :/ */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
    HAL_StatusTypeDef comStatus = HAL_I2C_Mem_Write(I2CHandler, devAddress, regAddress, I2C_MEMADD_SIZE_8BIT, values, size, calculateTimeout(size));
#pragma GCC diagnostic pop

    if(comStatus != HAL_OK)
    {
        retVal = false;
    }

    I2C_unlock();

    return retVal;
}
