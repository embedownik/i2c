/*
 * I2C.h
 *
 *      Author: Embedownik
 */

#ifndef I2CX_H_
#define I2CX_H_

#include <stdbool.h>
#include <stdint.h>

#include <board.h>

bool I2C_init(I2C_HandleTypeDef *handler);

bool I2C_readByte(uint8_t devAddress, uint8_t regAddress, uint8_t *value);

bool I2C_readBytes(uint8_t devAddress, uint8_t regAddress, uint8_t *value, uint8_t bytes);

bool I2C_writeByte(uint8_t devAddress, uint8_t regAddress, uint8_t value);

bool I2C_writeBytes(uint8_t devAddress, uint8_t regAddress, const uint8_t *values, uint16_t size);

#endif
